

API Requst Counts:

  Inventory Reconcilliation:
  --  [variants] = Total Stitch Unbundled Variant Count.
  --  [lookup max] = Drupal variable "stitch max variant lookup count" (default 50).
  --  [out of stock] = Total count of Drupal products with stock < 1.
  --  [purchase orders] = Total Stitch purchase orders that have not been completed.
  --  [variant requests] = [variants] / [lookup max] rounded up to a whole number.

  [variant requests] + ([out of stock] * (1 + [purchase orders]))

  For Example:
  -- [variants] = 75
  -- [lookup max] = 50
  -- [out of stock] = 4
  -- [purchase orders] = 10
  -- [variant requests] = 75 / 50 = 2 (rounded up to whole number)

  2 + (4 * (1 + 10)) = 46 requests

  Drupal Order:
  -- [line items] = Product line items on the Drupal order.

  10 + [line items]