<?php

/**
 * @file stitch.admin.inc
 */

/**
 * Form callback for the module configuration form.
 */
function stitch_config_form($form, $form_state) {
  $form['stitch_client_id'] = array(
    '#title' => t('Client ID'),
    '#description' => t('Stitchlabs Client ID, generated from their admin interface.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('stitch_client_id', ''),
  );
  $form['stitch_client_secret'] = array(
    '#title' => t('Client Secret'),
    '#description' => t('Stitchlabs Client Secret, generated from their admin interface.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('stitch_client_secret', ''),
  );

  $form['stitch_get_client_id'] = array(
    '#value' => t('Authenticate'),
    '#type' => 'submit',
  );

  // Allows the user to place Stitch into testing mode.
  $form['stitch_testing_mode_enabled'] = array(
    '#title' => t('Enable Test Mode'),
    '#description' => t('Test mode prevents API requests going out to Stitch.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('stitch_testing_mode_enabled', FALSE),
  );

  // Use Drupal's settings form builder.
  $form = system_settings_form($form);
  // Insert this after, so that values are saved before the redirect.
  $form['#submit'][] = 'stitch_config_form_submit';

  return $form;
}

/**
 * Submit callback for the module configuration form.
 */
function stitch_config_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Authenticate')) {
    $values = $form_state['values'];
    $_SESSION['stitchlabs_state'] = drupal_random_key();

    $arguments = array(
      'response_type' => 'code',
      // In order to manually encode it, Drupal's url "encoding" breaks Stitchlabs.
      'redirect_uri' => NULL,
      'client_id' => $values['stitch_client_id'],
      'scope' => '',
      'state' => $_SESSION['stitchlabs_state'],
    );
    $baseurl = 'https://api-pub.stitchlabs.com/authorize';
    $url = url($baseurl, array('query' => $arguments));
    // Circumvent Drupal's quasi urlencoding rules for URLs.
    $redirect_uri = urlencode(stitch_get_redirect_uri());
    $url = str_replace('redirect_uri&', "redirect_uri=$redirect_uri&", $url);

    drupal_goto($url);
  }

  // Save the testing mode form option.
  variable_set('stitch_testing_mode_enabled', $form_state['values']['stitch_testing_mode_enabled']);
}

/**
 * Utility function to provide a central way to generate a redirect uri.
 */
function stitch_get_redirect_uri() {
  return url(STITCH_REDIRECT_PATH, array('absolute' => TRUE));
}

/**
 * Menu callback hit by Stitch Labs.
 *
 * Stitch Labs redirects back with a temporary authentication code allowing us to
 * store a more permanent access token for future calls.
 */
function stitch_get_access_token() {
  $success = FALSE;

  // Make sure there are the appropriate parameters to start processing.
  if (!empty($_GET['code']) || !empty($_GET['error'])) {
    if (!empty($_GET['code']) && !empty($_GET['state']) && !empty($_SESSION['stitchlabs_state'])) {
      if ($_SESSION['stitchlabs_state'] == $_GET['state']) {
        // Load the Stitch library and fetch the access token.
        if (($library = libraries_load('StitchLabs')) && !empty($library['loaded'])) {
          $auth_code = $_GET['code'];
          $redirect_uri = stitch_get_redirect_uri();

          try {
            // Grab the actual requested info.
            $stitch = stitch_get_connection();
            // Allow for potential failures.
            if ($tokens = $stitch->authenticate($auth_code, $redirect_uri)) {

              // Save for later.
              variable_set('stitch_access_token', $tokens->access_token);

              drupal_set_message(t('Your access tokens were successfully retrieved.'));
            }
              $success = TRUE;
          }
          catch (Exception $e) {
            stitch_log_exception($e);
          }
        }
      }
    }

    unset($_SESSION['stitchlabs_state']);
  }

  // Notify if there is no access token.
  if (!$success && !empty($_SESSION['stitchlabs_state'])) {
    drupal_set_message(t('There was an error retrieving the access tokens from Stitch Labs, please try again.'), 'error');
  }

  drupal_goto('admin/config/services/stitchlabs');
}
