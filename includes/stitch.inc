<?php

/**
 * @file stitch.inc
 *
 * Provides implementation of the Stitchlabs library.
 */

/**
 * Helper function to return an authenticated StitchClient object.
 */
function stitch_get_connection() {
  if (($library = libraries_load('StitchLabs')) && !empty($library['loaded'])) {
    $client_id = variable_get('stitch_client_id');
    $client_secret = variable_get('stitch_client_secret');

    // Default to false in case these api creds havn't been authenticated yet.
    $access_token = variable_get('stitch_access_token', FALSE);

    $stitch = new StitchClient($client_id, $client_secret, $access_token);
    return $stitch;
  }
  else {
    return FALSE;
  }
}

/**
 * Utility function to retrieve all unbundled variants from Stitch
 * and queue their Drupal equivalent for update.
 */
function stitch_queue_product_inventory_update($stitch, $page = 1) {
  $variant = new Variant($stitch);
  $filters = array(
    'and' => array(
      array(
        'bundle' => 0,
      ),
    ),
  );

  // Set the number of variants to be retrieved in this call.
  $variant_count = variable_get('stitch_max_variant_lookup_count', 50);
  $result = $variant->lookUpFiltered($filters, $variant_count);
  if ($result->meta->last_page > $result->meta->current_page) {
    $page++;
    stitch_queue_product_inventory_update($stitch, $page);
  }

  // Loop through all variants and pair them up with their Drupal equivalent.
  $variants = $result->Variants;
  foreach ($variants as $variant_id => $variant_value) {
    $product = commerce_product_load_by_sku($variant_value->sku);
    if ($product) {
      $variant = $variant_value;
      $variant->stitch = $stitch;
      stitch_queue_product_stock_update($variant, $product);
    }
  }
}

/**
 * Utility function to get purchase orders for a given variant.
 */
function stitch_retrieve_variant_purchase_order($variant) {
  $stitch = $variant->stitch;
  try {


    $order = new PurchaseOrder($stitch);
    $filters = array(
      'and' => array(
        array('complete' => 0),
      ),
    );
    $orders = array();

    // Get a list of all purchase orders that have yet to be completed.
    $result = $order->lookUpFiltered($filters);
    $purchase_orders = $result->PurchaseOrders;

    // Loop through all purchase orders to find any that reference the variant.
    foreach ($purchase_orders as $purchase_order) {
      $result = $order->lookUpId($purchase_order->id);

      // Loop through all the variants referenced by the purchase order to see if
      // any of them match the variant being searched for.
      if (!empty($result->Variants)) {
        foreach($result->Variants as $variant_id => $variant_value) {

          // If the variant is in the purchase order, then add the purchase order
          // to be returned.
          if ($variant_id == $variant->id) {
            if (!empty($purchase_order->expected_date)) {
              $orders[$purchase_order->expected_date] = $purchase_order;
            }
          }
        }
      }
    }

    return $orders;
  }
  catch (Exception $e) {
    stitch_log_exception($e, __FUNCTION__, $variant, 'Variant');
  }
}

/**
 * Push a complete order to Stitchlabs.
 */
function stitch_order_push($stitch, $order) {
  try {
    // Reload the order to make sure that all values are up to date.
    $order = commerce_order_load($order->order_id);

    // Start off by creating/looking up the Stitch contact.
    $contact = stitch_lookup_create_contact($stitch, $order->uid, $order);

    // Create line item objects from the order.
    $line_items = stitch_create_line_items_from_order($order, $stitch);

    // Create an order to add the line items to.
    $stitch_order = new Order($stitch);

    // Check to see if this order has already been mapped to Stitch.
    // If not then proceed as usual.
    if ($stitch_mapping = stitch_mapping_object_load_by_drupal(
      'commerce_order', $order->order_id, 'Order')) {

      // This type of search can only return one result.
      $result = $stitch_order->lookUpId($stitch_mapping->stitch_id);

      // Set the values from the search against the stitch order.
      $stitch_order->setValues($result->SalesOrders[0]);
    }

    // Loop through all line item objects and add them to the order.
    foreach($line_items as $line_item) {
      $stitch_order->addLineItem($line_item);
    }

    $stitch_order->addContact($contact);
    stitch_map_values($stitch_order, 'Order', $order, $stitch);
    $stitch_order->save();


    // If the save was successful, then create a stich_mapping_object for it.
    if ($stitch_order->id) {
      stitch_mapping_object_create($stitch_order, $order->order_id, 'commerce_order');
      if (!empty($stitch_order->local_id)) {
        $order->order_number = $stitch_order->local_id;
      }
      commerce_order_save($order);
    }

    // Process the stitch invoice for this order.
    stitch_process_order_invoice($order);

    // If this order has failed a previous push attempt, then remove it from
    // the failed order tracking
    $failed_processes = variable_get('stitch_failed_processes', array());
    if (!empty($failed_processes[$order->order_id])) {
      unset($failed_processes[$order->order_id]);
      variable_set('stitch_failed_processes', $failed_processes);
    }
  }
  catch (Exception $e) {
    stitch_log_exception($e, __FUNCTION__, $order, 'Order');
  }
}

/**
 * Look up or create a stitch contact for the given Drupal User.
 */
function stitch_lookup_create_contact($stitch, $user_id, $order = FALSE) {
  $contact = new Contact($stitch);
  // Check to see if this user is already mapped to a stitch contact or person.
  if ($stitch_mapping = stitch_mapping_object_load_by_drupal('user', $user_id, 'Contact')) {

    // This type of search can only return one result.
    $result = $contact->lookUpId($stitch_mapping->stitch_id);

    // Save the values from the search result to the contact.
    $contact->setValues($result->Contacts[0]);
  }
  // Do a manual lookup of stitch contacts
  else {
    $filters = array();
    $wuser = entity_metadata_wrapper('user', $user_id);
    $user_name = $wuser->name->value();
    $filters['or'][] = array('name' => $user_name);

    if ($order) {
      $worder = entity_metadata_wrapper('commerce_order', $order);

      // Loop through all of the address profiles on the order, and set the
      // names on them as filters.
      $address_profiles = array('commerce_customer_billing', 'commerce_customer_shipping');
      foreach ($address_profiles as $profile_type) {
        $filter_value = $worder->{$profile_type}->commerce_customer_address->name_line->value();
        $filters['or'][] = array('name' => $filter_value);
      }
    }

    // Attempt to lookup the contact in Stitch Using the filters.
    $results = $contact->lookUpFiltered($filters);

    // Check to see if the search returned any contacts.
    // If more than one result is found, then just create a new Contact.
    // Not enough information to determine which contact should be used.
    if (!empty($results->Contacts) && count($results->Contacts) == 1) {

      // If only one contact matches, then return it.
      $contact->setValues($results->Contacts[0]);
    }
  }

   // Add additional values from the order.
  if ($order) {
    $worder = entity_metadata_wrapper('commerce_order', $order);
    $shipping_profile = $worder->commerce_customer_shipping->commerce_customer_address->value();
    $shipping_profile['shipping'] = 1;

    $billing_profile = $worder->commerce_customer_billing->commerce_customer_address->value();

    // Compare the street address of both profiles to see if they are the same.
    // If so, then just mark the shipping profile as both.
    if ($billing_profile['thoroughfare'] == $shipping_profile['thoroughfare']) {
      $shipping_profile['billing'] = 1;
    }
    else {
      $billing_profile['billing'] = 1;

      // Create a new address, map it, and add it to the contact.
      $address = new Address();
      stitch_map_values($address, 'Address', $billing_profile, $stitch);
      $contact->addAddress($address);
    }
    // Create a new address, map it, and add it to the contact.
    $address = new Address();
    stitch_map_values($address, 'Address', $shipping_profile, $stitch);
    $contact->addAddress($address);
  }

  // Now map the values of the contact
  $user = user_load($user_id);
  stitch_map_values($contact, 'Contact', $user, $stitch);

  $result = $contact->save();

  // If the save was successful, then create a stitch_mapping_object.
  if (!empty($contact->id)) {
    stitch_mapping_object_create($contact, $user_id, 'user');
  }

  return $contact;
}

/**
 * Create line items based on the products in the cart.
 */
function stitch_create_line_items_from_order($order, $stitch) {
  $worder = entity_metadata_wrapper('commerce_order', $order);

  $line_items = array();
  foreach($worder->commerce_line_items as $line_item_value) {
    if ($line_item_value->type->value() == 'product') {

      // @TODO: Provide default sku handling.
      $sku = '';
      drupal_alter('stitch_sku', $sku, $line_item);
      $variant = stitch_variant_sku_lookup($stitch, $sku);

      // Make sure that the variant exists in Stitch.
      if ($variant) {

        $line_item = stitch_new_variant_line_item($variant);

        stitch_map_values($line_item, 'LineItem', (array) $line_item_value->value(), $stitch);

        $line_items[] = $line_item;
      }
    }
  }
  return $line_items;
}

/**
 * Utility function to look up product variant based on the sku.
 */
function stitch_variant_sku_lookup($stitch, $sku) {
  $variant = new Variant($stitch);
  $filters = array(
    'and' => array(
      array(
        'sku' => $sku,
      ),
    ),
  );

  // Look up variant and return the first result.
  // SKUs are unique so there should only be one.
  $result = $variant->lookUpFiltered($filters);
  if (!empty($result->Variants)) {
    return $result->Variants[0];
  }
  return FALSE;
}

/**
 * Utility function to add a product variant as a line item to a Stitch Labs
 * order.
 */
function stitch_new_variant_line_item($variant) {
  // First create the line item using values from the variant.
  $variant_id = $variant->id;
  $description = $variant->auto_description;
  $line_item = new LineItem($variant_id, $description);

  return $line_item;
}

/**
 * Utility function to process invoices for an order.
 */
function stitch_process_order_invoice($order, $stitch = FALSE) {
  if (!$stitch) {
    $stitch = stitch_get_connection();
  }

  $invoice = new Invoice($stitch);

  if ($stitch_mapping = stitch_mapping_object_load_by_drupal('commerce_order', $order->order_id, 'Order')) {

    // Lookup the stitch order associated with this drupal order.
    $stitch_order = new Order($stitch);
    $stitch_order_lookup = $stitch_order->lookUpId($stitch_mapping->stitch_id);

    // Retrieve the order and invoice (if it exists) from the lookup.
    $stitch_order->setValues($stitch_order_lookup->SalesOrders[0]);

    // Don't attempt to update the order's invoice if it exists already.
    if (!isset($stitch_order_lookup->Invoices)) {

      $invoice->links->LineItems = $stitch_order->links->SalesOrderLineItems;
      foreach($invoice->links->LineItems as &$line_item) {
        $line_item->quantity = 1;
      }
    }

  }
  stitch_map_values($invoice, 'Invoice', $order, $stitch);
  $response = $invoice->save();

  stitch_process_order_invoice_payments($order, $response, $stitch);

}

/**
 * Utility function to process invoices and invoice paymetns for an order.
 */
function stitch_process_order_invoice_payments($order, $invoice_response, $stitch) {
  if ($order->data['commerce_payment_order_paid_in_full_invoked']) {

    if (!empty($invoice_response)) {
      $invoice = new Invoice($stitch);
      $stitch_order = new Order($stitch);

      $invoice_id = $invoice_response->Invoices[0]->id;
      $invoices = $invoice->lookUpId($invoice_id);
      $invoice->setValues(reset($invoices->Invoices));
      $orders = $stitch_order->lookUpId($invoice->links->SalesOrders[0]->id);
      $stitch_order->setValues($orders->SalesOrders[0]);

      $payment_amount = $stitch_order->total - $stitch_order->total_paid_balance;
      if ($payment_amount > 0) {
        $invoice_payment = new InvoicePayment($stitch);
        $invoice_payment->amount = $payment_amount;
        $invoice_payment->links->Invoices[] = array('id' => $invoice_id);
        stitch_map_values($invoice_payment, 'InvoicePayment', $order, $stitch);
        $invoice_payment->save();
      }
    }

  }
}

/**
 * Maps Drupal values to Stitch Object values based on the
 * configuration mapping.
 */
function stitch_map_values($object, $object_type, $drupal_values, $stitch) {
  $drupal_values = (array) $drupal_values;
  $config = stitch_get_config_mapping();
  // Limit the configuration to the fields for the current object type.
  $config = $config[$object_type] ?: FALSE;

  if ($config) {
    foreach ($config as $stitch_field => $drupal_field) {
      if (!empty($drupal_field)) {

        // If the field is an array, check if it has a callback before moving on.
        if (is_array($drupal_field)) {

          // If the field is an array and has a value attribute, then just set
          // the value of that attribute against the stitch field.
          if (!empty($drupal_field['value'])) {
            $return = $drupal_field['value'];
          }
          elseif (!empty($drupal_field['callback'])) {
            // Set the fields based on the callback and params set in the
            // configuration.
            $callback = $drupal_field['callback'];
            $params = $drupal_field['param'] ?: array();

            // Capture the return of the callback.
            $return = $callback($params, $drupal_values, $stitch);
          }

          // Only act on the return if it actually exists.
          if (!empty($return)) {

            // Allow the location to be overridden. Any additional field mapping
            // is taken care of in the callback.
            if (!empty($drupal_field['location'])) {
              $location = $drupal_field['location'];

              // The location can also specify a callback to place the child object,
              // rather than a property on the parent.
              if (is_array($location)) {
                if (!empty($location['callback'])) {
                  $location_callback = $location['callback'];
                  $object->{$location_callback}($return);
                }
              }
              else {
                // Allow for the location to be either an array or an object.
                if (is_array($object->{$location})) {
                  $object->{$location}[$stitch_field][] = $return;
                }
                else {
                  $object->{$location}->{$stitch_field}[] = $return;
                }
              }
            }
            else {
              $object->{$stitch_field} = $return;
            }
          }
        }
        elseif (is_object($drupal_values)
          && !empty($drupal_values->{$drupal_field})) {
          $object->{$stitch_field} = $drupal_values->{$drupal_field};
        }
        elseif (!empty($drupal_values[$drupal_field])) {
          $object->{$stitch_field} = $drupal_values[$drupal_field];
        }
      }
    }
  }
}

/**
 * Callback function to create a new object of the specified type and map its
 * values based on the configuration array.
 *
 * Note that this function is, in a sense, recursive as it calls the function that
 * called it.
 */
function _stitch_config_mapping_new_object($param, $drupal_values, $stitch) {
  $object_type = $param['object'];
  $object = new $object_type($stitch);

  // Map the values for this object so that it can be added to it's parent.
  stitch_map_values($object, $object_type, $drupal_values, $stitch);

  // Child objects shouldn't have their own instance of the stitch client.
  // If it is set, remove it.
  if (!empty($object->stitch)) {
    unset($object->stitch);
  }

  // Child objects that are being mapped shouldn't have their own id field.
  if (empty($object->id)) {
    unset($object->id);
  }
  return $object;
}

/**
 * Recursive function to look for a key in an array of unknown
 * depth/length.
 */
function _stitch_config_mapping_traverse_array($param, $values) {
  // If a parent is set, then only search within that item.
  if (!empty($param['parent'])) {
    $values = (array) $values;
    if (!empty($values[$param['parent']])) {
      $values = $values[$param['parent']];
    }
  }

  $destination = $param['destination'];

  if (!empty($values)) {
    foreach($values as $item_id => $item_value) {
      // If an identifier is set, then look for that before looking for the
      // destination.

      if ($item_id === $destination) {
        return $item_value;
      }

      elseif (is_array($item_value)) {
        $search = array(
          'destination' => $destination,
        );
        $result = _stitch_config_mapping_traverse_array($search, $item_value);
        // Check if the searched for item was found.
        if ($result !== FALSE) {
          // Destination was found.
          return $result;
        }
      }
    }
  }
}

/**
 * Recursive function to look for a key|value pair in an array of unknown
 * depth/length.
 */
function _stitch_config_mapping_array_find_identifier($param, $values) {
  $values = (array) $values;

  // If a parent is set, then only search within that item.
  if (!empty($param['parent'])) {
    // If wrapper setting are set, then load the value in an
    // entity_metadata_wrapper to load it's child elements.
    if (!empty($param['wrapper'])) {
      $wrapper = $param['wrapper'];
      $values = entity_metadata_wrapper($wrapper['type'], $values[$wrapper['id']]);
      $values = $values->{$param['parent']}->value();
    }
    else {
      $values = $values[$param['parent']];
    }
  }

  $destination = $param['destination'];
  $identifier = !empty($param['identifier']) ? $param['identifier'] : FALSE;

  foreach ($values as $item_id => $item_value) {
    if ($identifier) {
      // Check if this item is the right attribute to check off of.
      if ($item_id === $identifier['id']) {

        // If this is the right attribute, then check if the value matches
        // the value of the identifier.
        if ($item_value === $identifier['value']) {
          return $values;
        }
        else {
          // If this is not the right value, then stop searching this item
          // and move on.
          return FALSE;
        }
      }
      elseif (is_array($item_value)) {
        $search = array(
          'identifier' => $identifier,
          'destination' => $destination,
        );
        $result = _stitch_config_mapping_array_find_identifier($search, $item_value);
        // Check if the searched for item was found.
        if ($result !== FALSE) {
          // Destination was found.
          return $result;
        }
      }
    }
  }
}

/**
 * Wrapper function to traverse an array for a value and convert the result into
 * a decimal amount rather than an amount in cents.
 */
function _stitch_config_mapping_find_convert_amount($param, $values, $stitch) {
  $cents = _stitch_config_mapping_traverse_array($param, $values);
  $return = $cents / 100;
  return (string) $return;
}

/**
 * Wrapper function to traverse an array for a key|value pair. The result of that
 * search is searched again for a single key to return a value.
 * The value is then converted to a decimal.
 */
function _stitch_config_mapping_find_identifier_convert_amount($param, $values, $stitch) {

  $return = _stitch_config_mapping_array_find_identifier($param, $values);

  unset($param['parent']);
  return _stitch_config_mapping_find_convert_amount($param, $return, $stitch);
}

/**
 * Lookup function to return the id of the PersonFieldType that matches the filters.
 */
function _stitch_config_mapping_get_person_field_filtered($param, $values, $stitch) {

  $person_type = new PersonFieldType($stitch);
  $filters = array(
    'and' => array(
      array('field_type' => $param['field_type']),
      array('name' => $param['name']),
    ),
  );

  // Only one ID can be used, so just return the first one.
  // This should be adequate for most scenarios.
  $result = $person_type->lookUpFiltered($filters);
  return $result->PersonFieldTypes[0]->id;
}

/**
 * Callback function to get values for special line items such as shipping and
 * taxes.
 */
function _stitch_config_retrieve_special_line_item($param, $values, $stitch) {
  $type = $param['type'];

  try {
    $amount = 0;
    $worder = entity_metadata_wrapper('commerce_order', $values['order_id']);
    foreach ($worder->commerce_line_items as $line_item) {
      if ($line_item->type->value() == $type) {
        $amount += $line_item->commerce_total->amount->value();
      }
    }

    $amount = $amount / 100;
    return $amount;
  }
  catch (EntityMetadataWrapperException $e) {
    stitch_log_exception($e, __FUNCTION__, $values);
  }
}

/**
 * Callback function to get the tax rate for the current order.
 */
function _stitch_config_retrieve_order_taxrate($param, $values, $stitch) {
  $tax_rate = 0;
  drupal_alter('stitch_taxrate', $values, $tax_rate);
  $order = commerce_order_load($values['order_id']);

  if (!$tax_rate) {

    $order_totals = $order->commerce_order_total[LANGUAGE_NONE][0];
    foreach ($order_totals['data']['components'] as $component) {
      if ($component['name'] == 'base_price') {
        $subtotal = $component['price']['amount'];
      }
      if ($component['name'] == 'taxrates') {
        $tax_charged = $component['price']['amount'];
      }
    }

    if (!empty($subtotal) && !empty($tax_charged)) {
      $tax_rate = $tax_charged / $subtotal;
    }
  }
  // Stitch expects this as a whole percent.
  return $tax_rate * 100;

}

function _stitch_config_convert_timestamp($param, $values, $stitch) {
  if (!empty($param['field']) && !empty($values[$param['field']])) {
    $date = date('c', $values[$param['field']]);
    if ($date) {
      return $date;
    }
  }
}

function _stitch_config_mapping_lookup_id($param, $values, $stitch) {
  $drupal_id_field = $param['drupal_id_field'];

  $stitch_entity_type = $param['stitch_type'];
  $drupal_entity_type = $param['drupal_entity_type'];
  $entity_id = $values[$drupal_id_field];

  $mapping_object = stitch_mapping_object_load_by_drupal($drupal_entity_type, $entity_id, $stitch_entity_type, $reset = FALSE);
  $stitch_id = $mapping_object->stitch_id;

  return array('id' => $stitch_id);
}

/**
 * Helper function that returns the mapping array.
 */
function stitch_get_config_mapping() {
  $defaults = array(
    'Address' => array(
      'company' => 'organisation_name',
      'street1' => 'thoroughfare',
      'street2' => 'premise',
      'city' => 'locality',
      'state' => 'administrative_area',
      'zip' => 'postal_code',
      'country_iso' => 'country',
      'shipping' => 'shipping',
      'billing' => 'billing',
    ),
    'Contact' => array(
      'name' => 'name',
      'People' => array(
        'callback' => '_stitch_config_mapping_new_object',
        'location' => array(
          'callback' => 'addPerson',
        ),
        'param' => array(
          'object' => 'Person',
        ),
      ),
      'ContactTypes' => array(
        'location' => 'links',
        'value' => array(
          'id' => variable_get('stitch_contact_default_contact_type_id', 3689024),
        ),
      ),
    ),
    'LineItem' => array(
      'quantity' => 'quantity',
      'tax' => array(
        'callback' => '_stitch_config_retrieve_order_taxrate',
        'param' => array(),
      ),
      'price' => array(
        'callback' => '_stitch_config_mapping_find_convert_amount',
        'param' => array(
          'parent' => 'commerce_unit_price',
          'destination' => 'amount',
        ),
      ),
    ),
    'Order' => array(
      'subtotal_taxable' => array(
        'callback' => '_stitch_config_retrieve_special_line_item',
        'param' => array(
          'type' => 'product',
        ),
      ),
      'tax_percent' => array(
        'callback' => '_stitch_config_retrieve_order_taxrate',
        'param' => array(),
      ),
      'PricingTiers' => array (
        'location' => 'links',
        'value' => array(
          'id' => variable_get('stitch_order_default_pricing_tier_id', 1828484),
        ),
      ),
    ),
    'Invoice' => array(
      's_and_h' => array(
        'callback' => '_stitch_config_retrieve_special_line_item',
        'param' => array(
          'type' => 'shipping',
        ),
      ),
      'invoice_date' => array(
        'callback' => '_stitch_config_convert_timestamp',
        'param' => array(
          'field' => 'created',
        ),
      ),
      'payment_due_date' => array(
        'callback' => '_stitch_config_convert_timestamp',
        'param' => array(
          'field' => 'created',
        ),
      ),
      'SalesOrders' => array(
        'callback' => '_stitch_config_mapping_lookup_id',
        'location' => 'links',
        'param' => array(
          'drupal_entity_type' => 'commerce_order',
          'stitch_type' => 'Order',
          'drupal_id_field' => 'order_id',
        ),
      ),
    ),
    'InvoicePayment' => array(
      'InvoicePaymentTypes' => array(
        'location' => 'links',
        'value' => array(
          'id' => variable_get('stitch_payment_invoice_payment_type_id', 3016964),
        ),
      ),
      'payment_date' => array(
        'callback' => '_stitch_config_convert_timestamp',
        'param' => array(
          'field' => 'created',
        ),
      ),
    ),
    'Person' => array(
      'first_name' => array(
        'callback' => 'vgg_stitch_config_mapping_get_field_value',
        'param' => array(
          'field_name' => 'field_first_name',
          'attribute' => 'name',
        ),
      ),
      'last_name' => array(
        'callback' => 'vgg_stitch_config_mapping_get_field_value',
        'param' => array(
          'field_name' => 'field_last_name',
          'attribute' => 'name',
        ),
      ),
      'PersonFields' => array(
        'callback' => '_stitch_config_mapping_new_object',
        'location' => 'links',
        'param' => array(
          'object' => 'PersonField',
        ),
      ),
    ),
    'PersonField' => array(
      'value' => 'mail',
      'PersonFieldTypes' => array(
        'callback' => '_stitch_config_mapping_new_object',
        'location' => 'links',
        'param' => array(
          'object' => 'PersonFieldType',
        ),
      ),
    ),
    'PersonFieldType' => array(
      'id' => array(
        'callback' => '_stitch_config_mapping_get_person_field_filtered',
        'param' => array(
          'field_type' => 'email',
          'name' => 'home',
        ),
        'stitch' => TRUE,
      ),
    ),
  );

  drupal_alter('stitch_mapping_config', $defaults);

  return $defaults;
}
