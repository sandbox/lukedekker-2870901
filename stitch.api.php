<?php

/**
 * @file
 * These are the hooks that are invoked by the StitchLabs module.
 */

/**
 * @defgroup stitch_hooks Hooks provided by StitchLabs API
 * @{
 */

/**
 * @TODO: Really this process is way too complex. Several of the pieces should
 * be broken out into their own flow.
 *
 *
 * Allows modules to alter the configuration array before it is used to map a
 * Stitch object. Mapping goes here.
 *
 * This mapping is meant to be processed in a recursive fasion. Meaning that if
 * a mapping should create a new object, then the mapping will start all over
 * based on the new object, and then the new object will be mapped to the
 * original.
 *
 * Each first level key corresponds to an object defined in the StitchLabs_PHP
 * SDK.
 *
 * Second level items can be either a key => value pair, where the key is the
 * Stitch object key and the value is the drupal field, or an array containing
 * additional parameters for that item's mapping.
 *
 * If the second level item is an array, then it can contain several values:
 *
 *  callback: a function to call that will provide the values to be mapped
 *    against the stitch entity. In many cases, this will be a new stitch object
 *    (see _stitch_config_mapping_new_object()).
 *
 *  location: This allows you to specify where against the Stitch object the
 *    Drupal value will go. Many related Stitch objects (such as a ContactType
 *    for a Contact will be nested under a 'links' array). Location can also
 *    specify a callback, to be called against the parent object (such as the
 *    Contact->addPerson() method for a People Mapping).
 *
 *  value: Value will just set whatever it contains against the parent. This is
 *    useful for static values that will be the same for all objects of that
 *    type.
 *
 *  param: Param only matters if there is also a callback attribute. The values
 *    of param will be passed to the callback, to allow for one callback to be
 *    abstract.
 *
 * @param array $config
 *  Contains the defaults for mapping. Usually this will need to be heavily
 *  altered to suit your use case.
 */
function hook_stitch_mapping_config_alter(&$config) {

  $config = array(
    'Address' => array( // Based on fields from the Drupal Addressfield module.
      'company' => 'organisation_name',
      'street1' => 'thoroughfare',
      'street2' => 'premise',
      'city' => 'locality',
      'state' => 'administrative_area',
      'zip' => 'postal_code',
      'country_iso' => 'country',
      'shipping' => 'shipping',
      'billing' => 'billing',
    ),
    'Contact' => array(
      'name' => 'name',
      'People' => array(
        'callback' => '_stitch_config_mapping_new_object',
        'location' => array(
          'callback' => 'addPerson',
        ),
        'param' => array(
          'object' => 'Person',
        ),
      ),
      'ContactTypes' => array(
        'location' => 'links',
        'value' => array(
          'id' => 1234567,
        ),
      ),
    ),
    'LineItem' => array(
      'quantity' => 'quantity',
      'tax' => array(
        'callback' => '_stitch_config_retrieve_order_taxrate',
        'param' => array(),
      ),
      'price' => array(
        'callback' => '_stitch_config_mapping_find_convert_amount',
        'param' => array(
          'parent' => 'commerce_unit_price',
          'destination' => 'amount',
        ),
      ),
    ),
    'Order' => array(
      'subtotal_taxable' => array(
        'callback' => '_stitch_config_retrieve_special_line_item',
        'param' => array(
          'type' => 'product',
        ),
      ),
      'tax_percent' => array(
        'callback' => '_stitch_config_retrieve_order_taxrate',
        'param' => array(),
      ),
      'PricingTiers' => array (
        'location' => 'links',
        'value' => array(
          'id' => 1234567,
        ),
      ),
    ),
    'Invoice' => array(
      's_and_h' => array(
        'callback' => '_stitch_config_retrieve_special_line_item',
        'param' => array(
          'type' => 'shipping',
        ),
      ),
      'invoice_date' => array(
        'callback' => '_stitch_config_convert_timestamp',
        'param' => array(
          'field' => 'created',
        ),
      ),
      'payment_due_date' => array(
        'callback' => '_stitch_config_convert_timestamp',
        'param' => array(
          'field' => 'created',
        ),
      ),
      'SalesOrders' => array(
        'callback' => '_stitch_config_mapping_lookup_id',
        'location' => 'links',
        'param' => array(
          'drupal_entity_type' => 'commerce_order',
          'stitch_type' => 'Order',
          'drupal_id_field' => 'order_id',
        ),
      ),
    ),
    'InvoicePayment' => array(
      'InvoicePaymentTypes' => array(
        'location' => 'links',
        'value' => array(
          'id' => 1234567,
        ),
      ),
      'payment_date' => array(
        'callback' => '_stitch_config_convert_timestamp',
        'param' => array(
          'field' => 'created',
        ),
      ),
    ),
    'Person' => array(
      'first_name' => array(
        'callback' => 'your_own_callback_to_get_field_value',
        'param' => array(
          'field_name' => 'field_first_name',
          'attribute' => 'name',
        ),
      ),
      'last_name' => array(
        'callback' => 'your_own_callback_to_get_field_value',
        'param' => array(
          'field_name' => 'field_last_name',
          'attribute' => 'name',
        ),
      ),
      'PersonFields' => array(
        'callback' => '_stitch_config_mapping_new_object',
        'location' => 'links',
        'param' => array(
          'object' => 'PersonField',
        ),
      ),
    ),
    'PersonField' => array(
      'value' => 'mail',
      'PersonFieldTypes' => array(
        'callback' => '_stitch_config_mapping_new_object',
        'location' => 'links',
        'param' => array(
          'object' => 'PersonFieldType',
        ),
      ),
    ),
    'PersonFieldType' => array(
      'id' => array(
        'callback' => '_stitch_config_mapping_get_person_field_filtered',
        'param' => array(
          'field_type' => 'email',
          'name' => 'home',
        ),
      ),
    ),
  );
}

/**
 *
 * Allows other modules to alter the Stitch SKU before sending it off.
 *
 * The default SKU is empty, so it's crucial that this hook be invoked
 * by at least one other module.
 */
function hook_stitch_sku_alter(&$sku, $line_item) {
  // Processing to return the line item Stitch SKU.
}

/**
 * Allows modules to alter taxrates as they are being calculated.
 *
 * @param $order
 *  The commerce_order being sent to Stitch.
 *
 * @param $tax_rate
 *  The tax rate variable. Defaults to 0.
 */
function hook_stitch_taxrate_alter($order, &$tax_rate) {

}
/**
 * @} stitch_hooks
 */
