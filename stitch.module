<?php

/**
 * @file stitch.module
 *
 * Provides Drupal hooks to implement Stitch Labs order and inventory syncing.
 */

define('STITCH_BASE_PATH', 'admin/config/services');
define('STITCH_REDIRECT_PATH', STITCH_BASE_PATH . '/stitchlabs/callback');

/**
 * Implements hook_menu_alter().
 */
function stitch_menu_alter(&$items) {
  $items[STITCH_BASE_PATH . '/stitchlabs'] = array(
    'title' => t('Stitch Labs'),
    'description' => t('Configure the API keys for Stitch Labs.'),
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('stitch_config_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/stitch.admin.inc',
    'file path' => drupal_get_path('module', 'stitch'),
    'weight' => 1
  );
  $items[STITCH_REDIRECT_PATH] = array(
    'title' => t('Stitch Labs'),
    'description' => t('Forwarding location for Stitch Labs to send code.'),
    'type' => MENU_CALLBACK,
    'page callback' => 'stitch_get_access_token',
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/stitch.admin.inc',
    'file path' => drupal_get_path('module', 'stitch')
  );
  return $items;
}

/**
 * Implements hook_libraries_info().
 *
 * Add Stitch Labs PHP client library info.
 */
function stitch_libraries_info() {
  $libraries['StitchLabs'] = array(
    'name' => 'StitchLabs',
    'version callback' => 'stitch_skip_version_check',
    'files' => array(
      'php' => array(
        'StitchClient.php',
        'Order.php',
        'Contact.php',
        'Variant.php',
        'LineItem.php',
        'Address.php',
        'Person.php',
        'PersonField.php',
        'PersonFieldType.php',
        'PurchaseOrder.php',
        'Invoice.php',
        'InvoicePayment.php',
      ),
    ),
  );

  return $libraries;
}

/**
 * Simple function to ignore version checking for this Libraries API.
 */
function stitch_skip_version_check() {
  return TRUE;
}

/**
 * Implements hook_entity_info().
 */
function stitch_entity_info() {
  $return = array(
    'stitch_mapping_object' => array(
      'label' => t('Stitch Object Mapping'),
      'entity class' => 'StitchMappingObject',
      'controller class' => 'StitchMappingObjectController',
      'base table' => 'stitch_mapping_object',
      'revision table' => 'stitch_mapping_object_revision',
      'label callback' => 'entity_class_label',
      'fieldable' => FALSE,
      'exportable' => FALSE,
      'module' => 'stitch',
      'entity keys' => array(
        'id' => 'stitch_mapping_object_id',
        'label' => 'stitch_mapping_object_id',
        'revision' => 'revision_id',
      ),
      // Prevent Redirect alteration of the mapping object form.
      'redirect' => FALSE,
    ),
  );

  return $return;
}

/**
 * Wrapper function to determine if test mode is active.
 */
function stitch_test_mode_active() {
  return variable_get('stitch_test_mode_active', FALSE);
}

/**
 * Implements hook_mail()
 */
function stitch_mail($key, &$message, $params) {
  switch ($key) {
    case 'stitch_error':
    $subject = variable_get('stitch_mail_subject_stitch_error', 'Vert Groove Golf: Stitch Error');
    $message['subject'] = $subject;
    $body = variable_get('stitch_mail_body_stitch_error', 'There was an error syncing a Drupal order with stitch. Please notify the site administrator.');
    $message['body'][] = $body;
    break;
  }
}

/**
* Implements hook_cron_queue_info.
*/
function stitch_cron_queue_info() {
  $queues = array();
  $queues['stitch_update_inventory_from_stitch'] = array(
    'worker callback' => 'stitch_update_product_inventory',
    'time' => 120,
  );
  $queues['stitch_order_push'] = array(
    'worker callback' => 'stitch_proccess_order_push',
    'time' => 120,
  );

  return $queues;
}

/**
* Queue wrapper function. Queues inventory update from StitchLabs.
*/
function stitch_queue_product_stock_update($variant, $product) {
  if (!stitch_test_mode_active()) {

    $item = array('variant' => $variant, 'product' => $product);

    $queue = DrupalQueue::get('stitch_update_inventory_from_stitch');
    // There is no harm in trying to recreate existing.
    $queue->createQueue();

    $queue->createItem($item);
  }
}

/**
* Queue wrapper function. Queues the stitchlabs processing.
*/
function stitch_queue_order_push($order) {
  if (!stitch_test_mode_active()) {
    $queue = DrupalQueue::get('stitch_order_push');
    // There is no harm in trying to recreate existing.
    $queue->createQueue();

    $queue->createItem($order);
  }
}

/**
 * Worker callback to process an order push to StitchLabs.
 */
function stitch_proccess_order_push($order) {
  if ($stitch = stitch_init()) {
    stitch_order_push($stitch, $order);
  }
}

/**
 * Utility function to log any exceptions encountered.
 */
function stitch_log_exception($exception, $function, $values = array(), $values_label = 'Value') {

  $log = TRUE;

  // Only log orders if they have failed 3 times already. Otherwise, just throw
  // an exception to requeue it.
  if (is_object($values) && !empty($values->order_id)) {
    $failed_processes = variable_get('stitch_failed_processes', array());
    if (empty($failed_processes[$values->order_id])) {
      $failed_processes[$values->order_id] = 0;
    }
    $failed_processes[$values->order_id]++;
    if ($failed_processes[$values->order_id] >= 4) {
      unset($failed_processes[$values->order_id]);
      variable_set('stitch_failed_processes', $failed_processes);
    }
    else {
      variable_set('stitch_failed_processes', $failed_processes);
      $log = FALSE;
      $attempt_number = $failed_processes[$values->order_id];
      throw new Exception($exception . "<strong>Attempt #$attempt_number. Attempting again.</strong>");
    }
  }

  if ($log) {
    $values = print_r($values, TRUE);
    watchdog('stitch', 'Exception received from client: <pre>!exception<pre><p>' . $values_label . ': <pre>' . $values . '</pre></p><p>See ' . $function . '() <pre>' . $exception->getTraceAsString() . '</pre>', array('!exception' => $exception->getMessage()), WATCHDOG_ERROR);

    $email = variable_get('stitch_email_destination_stitch_error', 'vertgolf@clikfocus.com');
    drupal_mail('stitch', 'stitch_error', $email, LANGUAGE_NONE);
  }
}

/**
 * Utility function that lays the groundwork for connecting to StitchLabs.
 */
function stitch_init() {
  $stitch = FALSE;
  if (!stitch_test_mode_active()) {
    module_load_include('inc', 'stitch', 'includes/stitch');
    $stitch = stitch_get_connection();
  }
  return $stitch;
}

/**
 * Wrapper function to set the updated date during save
 */
function stitch_mapping_object_save($stitch_mapping_object) {

  $stitch_mapping_object->entity_updated = REQUEST_TIME;
  return $stitch_mapping_object->save();
}

/**
 * Create a new stitch mapping object based on the objects provided.
 *
 *
 * @param object $stitch_object
 *   Stitch object to link to.
 * @param int $entity_id
 *   Drupal id of the drupla entity to link to.
 * @param string $entity_type
 *   Entity type for the Drupal entity.
 *
 * @return StitchMappingObject
 *   Returns the created/loaded StitchMappingObject.
 *   Returns false if the object could not be created.
 */
function stitch_mapping_object_create($stitch_object, $entity_id, $drupal_entity_type) {
  $stitch_object_type = get_class($stitch_object);

  // Don't create this object if it already exists.
  // Just update it instead.
  $stitch_mapping_object = stitch_mapping_object_load_by_drupal($drupal_entity_type, $entity_id, $stitch_object_type);

  if (!$stitch_mapping_object) {
    // Create a new stitch_mapping object.
    $params = array(
      'stitch_id' => $stitch_object->id,
      'stitch_entity_type' => $stitch_object_type,
      'entity_id' => $entity_id,
      'drupal_entity_type' => $drupal_entity_type,
    );
    $stitch_mapping_object = entity_create('stitch_mapping_object', $params);
  }

  stitch_mapping_object_save($stitch_mapping_object);
  return $stitch_mapping_object;
}

/**
 * Return a Stitch Mapping Object.
 *
 * @see entity_load()
 */
function stitch_mapping_object_load($stitch_mapping_object_id, $conditions = array(), $reset = FALSE) {
  if (!is_bool($stitch_mapping_object_id)) {
    $stitch_mapping_object_id = array($stitch_mapping_object_id);
  }
  $objects = entity_load('stitch_mapping_object', $stitch_mapping_object_id, $conditions, $reset);
  return $objects ? reset($objects) : FALSE;
}


/**
 * Returns stitch object mappings for a given Drupal entity.
 *
 * @param string $entity_type
 *   Type of entity to load.
 * @param int $entity_id
 *   Unique identifier of the target entity to load.
 * @param string $stitch_entity_type
 *   Type of entity to load.
 * @param bool $reset
 *   Whether or not the cache should be cleared and fetch from current data.
 *
 * @return StitchMappingObject
 *   The requested stitchMappingObject or FALSE if none was found.
 */
function stitch_mapping_object_load_by_drupal($drupal_entity_type, $entity_id, $stitch_entity_type, $reset = FALSE) {
  $conditions = array(
    'entity_id' => $entity_id,
    'drupal_entity_type' => $drupal_entity_type,
    'stitch_entity_type' => $stitch_entity_type,
  );
  return stitch_mapping_object_load(FALSE, $conditions, $reset);
}

/**
 * Return stitch object mappings for a given stitch object.
 *
 * @param string $stitch_id
 *   Unique Id provided by stitch for the stitch record.
 * @param bool $reset
 *   Whether to reset the cache and retrieve a new record.
 *s
 * @return array
 *   Entities that match the given $stitch_id.
 */
function stitch_mapping_object_load_by_stitch_id($stitch_id, $reset = FALSE) {
  $conditions = array(
    'stitch_id' => $stitch_id,
  );
  return stitch_mapping_object_load(FALSE, $conditions, $reset);
}

/**
 * Wrapper function to kick off process of updating product stock with values
 * from Stitch.
 */
function stitch_update_inventory_from_stitch() {
  if ($stitch = stitch_init()) {
    stitch_queue_product_inventory_update($stitch);
  }
}

/**
 * Worker callback for stitch_update_product_inventory queue.
 */
function stitch_update_product_inventory($item) {
  if ($stitch = stitch_init()) {

    $wproduct = entity_metadata_wrapper('commerce_product', $item['product']);
    $variant = $item['variant'];

    // TODO: Add the expected_by field automatically on install.
    if (isset($wproduct->field_expected_by)) {

      // Only attempt to get the variant expected date if there are no items left and
      // the incoming stock is greater than the number of items already spoken for.
      if ($variant->stock < 1 && $variant->awaiting_stock > $variant->committed_stock) {
        $purchase_orders = stitch_retrieve_variant_purchase_order($variant);
        ksort($purchase_orders);

        $purchase_order = reset($purchase_orders);
        $expected_date = $purchase_order->expected_date;

        $wproduct->field_expected_by = strtotime($expected_date);
      }
    }

    $wproduct->commerce_stock = $variant->stock;

    // Save the product with the new values.
    $wproduct->save();
  }
}
